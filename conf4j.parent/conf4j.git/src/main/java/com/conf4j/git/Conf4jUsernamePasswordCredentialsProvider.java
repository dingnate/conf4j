/**
 * 
 */
package com.conf4j.git;

import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

/**
 * @author dingnate
 */
public class Conf4jUsernamePasswordCredentialsProvider extends UsernamePasswordCredentialsProvider {
	public Conf4jUsernamePasswordCredentialsProvider(String username, String password) {
		super(username, password);
	}

	@Override
	public boolean get(URIish uri, CredentialItem... items) throws UnsupportedCredentialItem {
		for (CredentialItem credentialItem : items) {
			// 首次ssh连接服务器时需要输入yesorno,自动设置yes
			if (credentialItem instanceof CredentialItem.YesNoType)
				((CredentialItem.YesNoType) credentialItem).setValue(true);
			else if (!super.get(uri, credentialItem))
				return false;
		}
		return true;
	}
}
