/**
 * 
 */
package com.conf4j.git.kit;

import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.ResetCommand.ResetType;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.merge.MergeStrategy;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.FetchResult;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import com.conf4j.git.Conf4jUsernamePasswordCredentialsProvider;
import com.conf4j.kit.StrKit;

/**
 * Git工具类
 * 
 * @author dingnate
 *
 */
public class GitKit {
	GitKit() {
	}

	public static UsernamePasswordCredentialsProvider getCredentialsProvider(String gituser, String gitpasswd) {
		if (StrKit.isBlank(gituser) || StrKit.isBlank(gitpasswd))
			return null;
		return new Conf4jUsernamePasswordCredentialsProvider(gituser, gitpasswd);
	}

	public static void doCloneOrUpdate(String gituri, CredentialsProvider credentialsProvider, File gitDir,
			String branch) throws GitAPIException, IOException {
		if (new File(gitDir, ".git").exists())
			// 防止客户机修改配置导致与配置仓库不一致，修改为强制替换配置
			// doPull(gitDir, credentialsProvider, MergeStrategy.THEIRS);
			doForceUpdate(gitDir, credentialsProvider);
		else
			doClone(gitDir, gituri, credentialsProvider, branch);
	}

	public static void doClone(File gitDir, String gituri, CredentialsProvider credentialsProvider, String branch)
			throws GitAPIException, IOException {
		if (!gitDir.exists())
			gitDir.mkdirs();
		if (branch == null)
			branch = Constants.HEAD;
		Git.cloneRepository().setURI(gituri).setCredentialsProvider(credentialsProvider).setDirectory(gitDir)
				.setBranch(branch).call().close();
	}

	public static FetchResult doForceUpdate(File gitDir, CredentialsProvider credentialsProvider) throws IOException,
			GitAPIException {
		Git git = null;
		try {
			git = Git.open(gitDir);
			FetchResult ret = git.fetch().setCredentialsProvider(credentialsProvider).call();
			git.reset().setMode(ResetType.HARD).setRef(Constants.FETCH_HEAD).call();
			return ret;
		} finally {
			if (git != null)
				git.close();
		}
	}

	public static PullResult doPull(File gitDir, CredentialsProvider credentialsProvider, MergeStrategy strategy)
			throws IOException, GitAPIException {
		Git git = null;
		try {
			git = Git.open(gitDir);
			if (strategy == null)
				strategy = MergeStrategy.RECURSIVE;
			return git.pull().setStrategy(strategy).setCredentialsProvider(credentialsProvider).call();
		} finally {
			if (git != null)
				git.close();
		}
	}
}
