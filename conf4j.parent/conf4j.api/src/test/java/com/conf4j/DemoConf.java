package com.conf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.conf4j.Conf;
import com.conf4j.annotation.Config;
import com.conf4j.kit.ConfKit;

@Config(file = "demo.properties", prefix = "demo")
public class DemoConf implements Conf {
	private static transient final Logger LOG = LoggerFactory.getLogger(Conf.class);
	public final static DemoConf ME = new DemoConf();
	static {
		try {
			ConfKit.handler(DemoConf.class);
		} catch (Exception e) {
			LOG.error(String.format("handler %s failed.", DemoConf.class.getName()), e);
		}
	}
	String attrStr;
	long attrLong;
	int attrInt;
	boolean attrBool;

	/**
	 * @return the attrStr
	 */
	public String getAttrStr() {
		return attrStr;
	}

	/**
	 * @param attrStr the attrStr to set
	 */
	public void setAttrStr(String attrStr) {
		this.attrStr = attrStr;
	}

	/**
	 * @return the attrLong
	 */
	public long getAttrLong() {
		return attrLong;
	}

	/**
	 * @param attrLong the attrLong to set
	 */
	public void setAttrLong(long attrLong) {
		this.attrLong = attrLong;
	}

	/**
	 * @return the attrInt
	 */
	public int getAttrInt() {
		return attrInt;
	}

	/**
	 * @param attrInt the attrInt to set
	 */
	public void setAttrInt(int attrInt) {
		this.attrInt = attrInt;
	}

	/**
	 * @return the attrBool
	 */
	public boolean isAttrBool() {
		return attrBool;
	}

	/**
	 * @param attrBool the attrBool to set
	 */
	public void setAttrBool(boolean attrBool) {
		this.attrBool = attrBool;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DemoConf [attrStr=" + attrStr + ", attrLong=" + attrLong + ", attrInt=" + attrInt + ", attrBool="
				+ attrBool + "]";
	}
}
