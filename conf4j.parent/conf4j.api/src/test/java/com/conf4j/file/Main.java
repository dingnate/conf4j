package com.conf4j.file;

import java.util.Properties;

import com.conf4j.ConfigManager;
import com.conf4j.DemoConf;

/**
 * @author dingnate
 *
 */
public class Main {
	public static void main(String[] args) throws InterruptedException {
		// 通过配置类的me获取实例调用配置属性
		System.out.println(DemoConf.ME);
		// 通过ConfigManager.config(配置类)获取实例调用配置属性
		System.out.println(ConfigManager.config(DemoConf.class));
		// 通过ConfigManager.putProperties()追加自定义全局配置属性
		Properties properties = new Properties();
		String testPropertyKey = "test.add.property.key";
		properties.setProperty(testPropertyKey, "test.add.property.value");
		ConfigManager.putProperties(properties);
		System.out.println(testPropertyKey + "=" + ConfigManager.getValueByKey(testPropertyKey));
		
		//启动配置扫描
		ConfigManager.startScaner();
		int i = 0;
		while (i++ < 100) {
			Thread.sleep(2000);
			System.out.println(DemoConf.ME);
		}
	}
}
