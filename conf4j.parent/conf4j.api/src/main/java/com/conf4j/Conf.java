package com.conf4j;

import com.conf4j.annotation.Config;



/**
 * 配置接口类<br><br>
 * 
 * 子类使用{@link Config}注解时，必须有如下定义<br>
 * <code>
 * public final static SubClass ME = new SubClass();<br>
 * </code>
 * 
 * @author dingnate
 *
 */
public interface Conf {
	String SINGLETON_NAME = "ME";
	String SEPARATOR = ",";
}
