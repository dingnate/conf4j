package com.conf4j.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 配置类自动映射注解
 * 
 * @author dingnate
 *
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface Config{
	/**
	 * key的前缀。如：a.b.c=hello,prefix="a.b"
	 * 
	 * @return
	 */
	String prefix();

	/**
	 * 配置文件的路径
	 * 
	 * @return
	 */
	String file() default "";
}
