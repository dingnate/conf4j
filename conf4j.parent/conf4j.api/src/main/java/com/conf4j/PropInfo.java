package com.conf4j;

import java.io.File;
import java.util.Properties;

import com.conf4j.kit.ConfKit;

/**
 * 配置文件的信息 以及 其版本号 只要本地文件发送变化，版本号就不一样
 * 
 * @author dingnate
 */
public class PropInfo {
	private String version;
	private Properties properties;

	public PropInfo() {
	}

	public PropInfo(String version, Properties properties) {
		this.version = version;
		this.properties = properties;
	}

	public PropInfo(File file) {
		version = ConfKit.getVersion(file);
		properties = new Prop(file).getProperties();
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}

	public String getString(Object key) {
		return (String) getProperties().get(key);
	}
}