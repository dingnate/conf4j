package com.conf4j.svn;

import java.util.Properties;

import com.conf4j.Conf4jConfigScanerConf;
import com.conf4j.ConfigManager;

/**
 * @author dingnate
 *
 */
public class Main {
	public static void main(String[] args) throws InterruptedException {
		Properties properties = new Properties();
		String testPropertyKey = "test.add.property.key";
		properties.setProperty(testPropertyKey, "test.add.property.value");
		ConfigManager.putProperties(properties);
		System.out.println(testPropertyKey + "=" + ConfigManager.getValueByKey(testPropertyKey));
		Conf4jConfigScanerConf config = ConfigManager.config(Conf4jConfigScanerConf.class);
		ConfigManager.startScaner();//启动配置扫描
		int i = 0;
		while (i++ < 100) {
			Thread.sleep(2000);
			System.out.println(config);
		}
	}
}
